# thread_pool_lf

Kind of minimalistic thread pool implementation which can be considered to be lock-free (in case if submitted functions do not break the property). Also standard allocator is used which makes the thread pool in some sense non lock-free.
