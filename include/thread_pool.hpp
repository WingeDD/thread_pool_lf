#pragma once

#include <iostream>
#include <unistd.h>
#include <queue>
#include <unordered_map>
#include <functional>
#include <thread>
// #include <mutex>
#include <future>
#include "boost/lockfree/queue.hpp"


namespace tp {
    class ThreadPool {
        private:
            std::atomic<unsigned short> workers_count;
            std::atomic<unsigned int> worker_refresh_time;
            std::atomic<int> workers_remove_counter;
            // std::queue<std::function<void()>> tasks;
            // std::mutex tasks_mx;
            boost::lockfree::queue<std::function<void()>*> tasks;

            void thread_job();

        public:
            ThreadPool(unsigned short workers_count = std::thread::hardware_concurrency(), unsigned int worker_refresh_time = 5u);
            ~ThreadPool();
            void set_worker_refresh_time(unsigned int worker_refresh_time);
            void add_workers(unsigned short workers_count);
            void remove_workers(unsigned short workers_count);

            template <typename F, typename... A>
            void push_task(F&& task, A&&... args) {
                // std::function<void()> task_function = std::bind(std::forward<F>(task), std::forward<A>(args)...);
                // std::lock_guard lock(tasks_mx);
                // tasks.push(task_function);
                std::function<void()>* task_function_ptr = new std::function<void()>;
                *task_function_ptr = std::bind(std::forward<F>(task), std::forward<A>(args)...);
                tasks.push(task_function_ptr);
            }

            template <typename F, typename... A, typename R = std::invoke_result_t<std::decay_t<F>, std::decay_t<A>...>>
            std::future<R> submit(F&& task, A&&... args) {
                std::function<R()> task_function = std::bind(std::forward<F>(task), std::forward<A>(args)...);
                std::shared_ptr<std::promise<R>> task_promise = std::make_shared<std::promise<R>>();
                push_task(
                    [task_function, task_promise]
                    {
                        try {
                            if constexpr (std::is_void_v<R>) {
                                std::invoke(task_function);
                                task_promise->set_value();
                            } else {
                                task_promise->set_value(std::invoke(task_function));
                            }
                        }
                        catch (...) {
                            try {
                                task_promise->set_exception(std::current_exception());
                            }
                            catch (...) {
                            }
                        }
                    }
                );
                return task_promise->get_future();
            }
        
            void wait_all() const;
    };
}
