#include "thread_pool.hpp"

namespace tp {
    void ThreadPool::thread_job() {
        while (true) {
            bool got_task(false);
            std::function<void()>* task_ptr;
            got_task = tasks.pop(task_ptr);
            // std::function<void()> task;
            // {
            //     std::lock_guard lock(tasks_mx);
            //     if (!tasks.empty()) {
            //         task = tasks.front();
            //         tasks.pop();
            //         got_task = true;
            //     }
            // }
            if (got_task) {
                try {
                    (*task_ptr)();
                } catch (...) {
                    // handle_exception(std::current_exception());
                }
                delete task_ptr;
            } else {
                int wrc_before_decrement = workers_remove_counter--;
                if (wrc_before_decrement > 0) {
                    workers_count--;
                    return;
                } else {
                    workers_remove_counter++;
                }

                unsigned int sleep_time = worker_refresh_time.load();
                if (sleep_time) {
                    std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));
                } else {
                    std::this_thread::yield();
                }
            }
        }
    }

    ThreadPool::ThreadPool(unsigned short workers_cnt, unsigned int worker_refresh_time):
        workers_count(workers_cnt), worker_refresh_time(worker_refresh_time), workers_remove_counter(0), tasks(0) {
        for (unsigned short i = 0; i < workers_cnt; ++i) {
            std::thread t(std::mem_fn(&ThreadPool::thread_job), this);
            t.detach();
        }
    }

    ThreadPool::~ThreadPool() {
        wait_all();
        remove_workers(std::numeric_limits<unsigned short>::max());
        while (workers_count.load() > 0) {
            std::this_thread::yield();
        }
    }

    void ThreadPool::set_worker_refresh_time(unsigned int worker_refresh_time) {
        this->worker_refresh_time.store(worker_refresh_time);
    }

    void ThreadPool::add_workers(unsigned short workers_cnt) {
        for (unsigned short i = 0; i < workers_cnt; ++i) {
            workers_count++;
            std::thread t(std::mem_fun(&ThreadPool::thread_job), this);
            t.detach();
        }
    }

    void ThreadPool::remove_workers(unsigned short workers_cnt) {
        // threads will process workers_remove_counter and workers_count themselves
        workers_remove_counter.fetch_add(static_cast<int>(workers_cnt));
    }

    void ThreadPool::wait_all() const {
        while (!tasks.empty()) {
            std::this_thread::yield();
        }
    }
}
