#include <iostream>
#include "thread_pool.hpp"

// using namespace std::chrono_literals;

int main() {
    tp::ThreadPool thread_pool(4, 0);
    auto start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < 10000; ++i) {
        thread_pool.submit(
            [i](){
                int t = rand() % 10;
                std::this_thread::sleep_for(std::chrono::milliseconds(t));
                std::cout <<
                "thread_id: " << std::this_thread::get_id() <<
                "; task_number: " << i <<
                "; worked for (ms): " << t <<
                std::endl;
            }
        );
    }
    thread_pool.wait_all(); // all tasks are consumed but not all of them are already executed
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Duration: " << duration.count() << " ms" << std::endl;
}
